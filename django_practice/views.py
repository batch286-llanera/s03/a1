from django.http import HttpResponse
from django.shortcuts import render, redirect

from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict


from .models import GroceryItem


def index(request):
	groceryitem_list = GroceryItem.objects.all()
	context = {
		'groceryitem_list' : groceryitem_list,
		'user': request.user
		}
	return render(request, "django_practice/index.html", context)

def groceryitem(request, groceryitem_id):
	# response = " You are viewing the details of %s"
	# return HttpResponse(response %groceryitem_id)
	todoitem = model_to_dict(GroceryItem.objects.get(pk=groceryitem_id))
	return render(request, "django_practice/groceryitem.html", groceryitem)

def register(request):

    users = User.objects.all()
    is_user_registered = False
    context = {
        "is_user_registered": is_user_registered
    }

    for indiv_user in users:
        if indiv_user.username == "janedoe":
            is_user_registered = True
            break

    if is_user_registered == False:
        user = User()
        user.username = "janedoe"
        user.first_name = "Jane"
        user.last_name = "Doe"
        user.email = "jane@mail.com"
        user.set_password("jane1234")
        user.is_staff = False
        user.is_active = True
        user.save()
        context = {
            "first_name": user.first_name,
            "last_name": user.last_name
        }

    return render(request, "django_practice/register.html", context)


def change_password(request):

    is_user_authenticated = False

    user = authenticate(username="janedoe", password="jane1234")
    print(user)
    if user is not None:
        authenticated_user = User.objects.get(username='janedoe')
        authenticated_user.set_password("janedoe1")
        authenticated_user.save()
        is_user_authenticated = True
    context = {
        "is_user_authenticated": is_user_authenticated
    }

    return render(request, "django_practice/change_password.html", context)


def login_view(request):
    username = "janedoe"
    password = "janedoe1"
    user = authenticate(username=username, password=password)
    context = {
        "is_user_authenticated": False
    }
    print(user)
    if user is not None:
        login(request, user)
        return redirect("index")
    else:
        return render(request, "django_practice/login.html", context)

def logout_view(request):
    logout(request)
    return redirect("index")



